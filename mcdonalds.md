# CMS - Content Management System.

## Pràctica 2: 

Seleccioneu una empresa real que tingui similitud amb algun dels tres casos que heu treballat a la pràctica de cerca d'informació (Restaurant, Botiga online, Startup de projectes).

Quina empresa heu seleccionat?

* Jofre: SquareEnix (Squaresoft)

* Kilian: Steam

* Uiliam: Blizzard

* Santi: Cooler Master

* Dani: McDonalds

* Manu: VelscoTattoo

Un cop seleccioneu la marca/empresa podeu instal·lar Wordpress i començar a personalitzar la web amb les opcions que heu investigat (plugins).

**Obligatori:** Mostrar captura i/o explicació. 

* Canviar el Tema per defecte instal·lant-ne un.

* Instal·lar 5 plugins (justificant perquè els instal·leu).

![Alt](images/mcdonalds1.png)

Central color palette y el editor clásico han sido seleccionados para mejorar el aspecto de la página web. Collapse-o-matic para poder crear desplegables. Los plugins de video han sido para poder agregar contenido de youtube, y por último el Wordfence Security para agregar seguridad a la página.

* Crear un perfil de cada tipus que permeti Wordpress. Explicar què pot fer cada tipus de perfil.

![Alt](images/mcdonalds2.png)

**Administrador**: Tiene todos los permisos para poder editar, borrar y agregar cualquier cosa del WordPress.

**Autor**: Los autores pueden agregar entradas y editarlas y/o borrarlas.

**Colaborador**: Este tipo de usuario solo puede editar las entradas a las que se les haya dado acceso.

**Editor**: El editor puede gestionar el contenido del sitio, moderando comentarios y tags, etc.

**Suscriptor**: Solo puede leer las entradas y páginas públicas del sitio.

* Canvia o afegeix algun canvi CSS a un Tema. Com ho has fet?

![Alt](images/mcdonalds3.png)

Dentro del menú de edición de nuestra página, accedemos al menú "CSS adicional>" y ahí escribimos el texto que vayamos a editar.

