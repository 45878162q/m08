# Curriculum Vitae
![Alt](images/fotoperfil.jpg "Title")

# Datos Personales
* **Nombre:** Daniel Candelario Roldán
* **Dirección:** C/ Cantàbria nº73
* **Teléfono:** 683 51 57 05
* **E-mail:** danicandelario13@gmail.com

# Actualmente
* **Estudiando:** 2º año de SMIX

# Estudios 
* ESO
* [Certificado en SO Linux essentials](images/LE-1.pdf "")

# Conocimientos informáticos
* Conocimientos básicos de Python
* Conocimientos medios de App Inventor
* Conocimientos medios en el paquete ofimático LibreOffice
* Conocimientos medios en VirtualBox
* Conocimiento en instalación de Sistemas Operativos
* Usuario constante de Linux

# Idiomas
| Idioma | Compresión oral | Expresión oral | Expresión escrita |
|--------|-----------------|----------------|-------------------|
|Castellano|Natal|Natal|Natal|
|Catalán|Alto|Alto|Alto|
|Inglés|Medio-alto|Medio|Medio|
# Futuro y aspiraciones

- [x] Participar en un evento educativo
- [ ] Realizar un ciclo superior DAM
- [ ] Aprender programación en JAVA
- [ ] Conseguir certificados avanzados de Linux

# Capacidades transversales
* Sociable
* Dinámico
* Adaptable
* Capacidad de aprendizaje
* Liderazgo
* Responsable