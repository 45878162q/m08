Ara que teniu: 

- Elements per a crear un curs de la p1.

- Un Moodle instal·lat a una MV.

___

El següent pas és crear el vostre curs a Moodle, seguiu els passos del vídeo i creeu un curs amb:

- Títol del vostre curs.

![Alt](images/moodle1.png)

- Recurs del curs: Un document pdf

![Alt](images/moodle2.png)

- Recurs del curs: Un enllaç a una web o video extern al moodle.

![Alt](images/moodle3.png)

- Una entrega d'una pràctica amb l'enunciat en Libreoffice write (.odt).

![Alt](images/moodle4.png)

- Un qüestionari amb 10 preguntes.

![Alt](images/moodle6.png)

![Alt](images/moodle7.png)

Justifiqueu els apartats amb captures adients. 