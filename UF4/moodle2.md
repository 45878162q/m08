## 1.-crear copia de seguretat del curs de Moodle de la pràctica anterior.

![Alt](images/copiaseguridad1.png)

![Alt](images/copiaseguridad2.png)

## 2.-crear un compte gratuït a https://moodlecloud.com/

![Alt](images/moodlecloud1.png)

## 3.-restaurar la copia de seguretat del curs local al curs de moodlecloud.

![Alt](images/copiaseguridad3.png)