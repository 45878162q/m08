En tu curso de moodle (online o local):



-  Crea 2 usuarios, uno llamado profesorX (donde X es tu apellido), otro llamado alumnoX, donde alumno es tu apellido.

![Alt](images/moodle8.png)

-  Haz a profesorX profesor de tu curso y a alumnoX alumno del curso.

![Alt](images/moodle9.png)

-  Haz una entrega como alumno del curso (de la actividad que ya tenías creada).

![Alt](images/moodle10.png)

-  Corrige una entrega como profesor del curso.

![Alt](images/moodle11.png)

-  Instala la extensión (plugin) attendance.

![Alt](images/moodle12.png)

-  Añade attendance a tu curso.

![Alt](images/moodle13.png)

-  Crea una sesión de lunes a viernes de 9 a 10.

![Alt](images/moodle14.png)

-  Pasa lista en la sesión de un día.

![Alt](images/moodle15.png)

-  Crea un fòrum al teu curs anomenat "Consultes".

![Alt](images/moodle16.png)

-  Crea, com alumne, una consulta. L'hauràs de respondre com a professor.

![Alt](images/moodle17.png)
  
