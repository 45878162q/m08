# SilveOS

[SilveOS](http://www.silveos.com/) es un sistema operativo web de Microsoft Silverlight. Ofrece una gran comodidad, 
ya que es muy parecido a Windows. Tiene una interfaz muy familiar, y te da la sensación 
como si estuvieras trabajando en tu ordenador de casa. 

Toda tu información es almacenada en la nube, por lo tanto no es necesario guardar
ningún archivo en local. También, Puedes ejecutar aplicaciones sin necesidad de descargarlas.

![images](images/silveos.png)
___
**Requisitos:** 
- Para Ejecutarse necesita [Microsoft Silverligth](https://www.microsoft.com/silverlight/).
- Sistema operativo Windows.
- Conexión a internet.

### Características
Las aplicaciones que están disponibles en SilveOS són:
- Explorador de archivos
- Buscador de Internet
- Reproductor de Video
- Editor de [texto enriquecido](https://es.wikipedia.org/wiki/Texto_enriquecido)
- [Lector RSS](https://www.rss.nom.es/lector-rss/)
- Bloc de notas
- Paint
- Cliente de Twitter
- Visualizador de [Flickr](https://es.wikipedia.org/wiki/Flickr) y YouTube
- [Tierra Virtual](https://es.wikipedia.org/wiki/Bing_Maps_Platform)
- Chat
- Calculadora
- Juegos variado (Ajedrez, Solitario, Buscaminas, ...)
- Gadgets como Reloj y calendario

### Ventajas
- Sus archivos y aplicaciones son accesibles desde cualquier ordenador del mundo
- No necesitas instalar software ni actualizarlo, por lo tanto no tienes por qué preocuparte de malware.
- Seguridad ante virus, gusanos, spyware, etc.
- Tiene un escritorio personalizable, con muchos temas y wallpapers, e incluso fotos propias del usuario.
- No necesitas tener grandes recursos para poder utlizarlo.
- Estando en Internet, te permite comunicarte y entretenerte con las diferentes apps web
- Es completamente gratuito, y no es necesario registrarse, se puede utilizar con una cuenta Guest
- Tiene disponible una versión disponible para Windows Phone

## Desventajas
- En la versión web, su principal y más importante desventaja es que algunas de las aplicaciones están desactualizadas, no son funcionales y tienen interfaces muy extrañas.
- Es necesario tener conexión a internet para poder utilizarlo.
- Solo cuenta con las aplicaciones más básicas al inicio.
