# Lo básico

En el primer contacto con Apache vas a hacer lo siguiente:

1. Crearás un directorio llamado **/var/www/html/amazon**
    * amazon debería ser propiedad de **www-data**
    
2. Cuando alguien escriba en el navegador de tu MV **amazon.poblenou** harás que sirva tu fichero de inicio del proyecto de amazon, que debería llamarse **index.html** para facilitar las cosas.

    * DocumentRoot /var/www/html/amazon
    * ServerName amazon.poblenou
    
    
# Directivas de Apache

Ahora que ya sabes dónde debes escribir para hacer *magia* con Apache usarás la documentación oficial de Apache2.

[Documentación Apache](https://httpd.apache.org/docs/2.4/es/mod/directives.html)

**Muestra una captura de pantalla de los cambios en los ficheros y del navegador con el resultado**
Siguiendo esta documentación deberás adaptar tu configuración de servidor virtual *Virtual host* para que:
 
* Cuando introduzcas en el navegador "amazon.poblenou/imagenes" se muestren la imágenes que tengas en el directorio "/var/www/html/Imagenes"
 
![Alt](images/amazon2.png)
    
* Copia tres imágenes en ese directorio para que se muestre el listado por el navegador.

![Alt](images/amazon1.png)

* Ahora vamos a hacerlo más difícil: Cuando introduzcas en el navegador "amazon.poblenou/galeria" se muestren la imágenes que tengas en el directorio "/home/TUUSUARIO/galeria"
  **NOTA:** Para que esto te funcione deberás cambiar los permisos y usuario del directorio galeria y añadir el directorio en el fichero de configuración de Apache.

![Alt](images/amazon3.png)

![Alt](images/amazon4.png)

* Copia tres imágenes en ese directorio para que se muestre el listado por el navegador.

![Alt](images/amazon5.png)

* Cambia el fichero de configuración para que cuando se introduzca en el navegador "amazon.poblenou/galeria" salga la pantalla de que no hay acceso al recurso, mensaje de error 403 Forbidden.* 
 
![Alt](images/amazon6.png)

![Alt](images/amazon7.png)

* Ahora cambiarás la configuración para que no se pueda acceder a "amazon.poblenou/galeria" desde la ip del servidor pero sí desde cualquier otra.

![Alt](images/amazon8.png)

![Alt](images/amazon9.png)

* Por último cambia el fichero de configuración para que solo puedan acceder a "amazon.poblenou/galeria" desde el mismo ordenador donde está el servidor instalado.

![Alt](images/amazon10.png)

Resulta que queremos hacer que el servidor Apache responda a peticiones sobre el puerto **8080**, además del puerto habitual 80.

* Cambia la configuración de Apache para que cumpla esta condición.
    
![Alt](images/amazon12.png)

![Alt](images/amazon11.png)

* Comprueba que se puede acceder a tu servidor desde el puerto 8080 (nombreserver:8080) en un navegador.

![Alt](images/amazon13.png)

* Cambia tu Virtual Host para que en el puerto 8080 muestre un directorio diferente al que muestras por el puerto 80, podría ser el directorio "/var/www/html/puertosgrises" y en él pondrás una página Web en HTML que indique claramente que se está accediendo por el puerto 8080.

![Alt](images/amazon14.png)

![Alt](images/amazon15.png)

Por último dejad el servidor respondiendo simplemente peticiones al puerto 80 y con vuestro directorio de amazon accesible sin más.
    
# Preguntas:

1. ¿En qué directorio se encuentran los *Virtual hosts* activos en Apache2?

/etc/apache/sites-enabled

2. ¿Con qué instrucción de Apache habilitamos el fichero de configuración "miconfiguracion.conf" de un *Virtual host*?

sudo a2ensite miconfiguracion.conf

3. ¿En qué fichero de configuración ponemos los puertos de escucha del servidor Apache?

En el /etc/apache2/**ports.conf**

4. ¿Cual es el puerto que se utiliza para acceder por https a un servidor web?

Por defecto es el puerto 443