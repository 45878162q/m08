Aquesta pràctica té 2 apartats:

1.- Instal·leu Thunderbird a la mateixa MV on vau instal·lar Postfix i configureu-lo perque un compte de correu electrònic ja creat a i utilitzat amb Squirrelmail funcioni amb Thunderbird (mostreu com envieu i rebeu mails).

![Alt](images/thunderbird2_1.png)

2.-Busqueu posibles vulnerabilitats al servidor de mail Postfix i contesteu amb les vostres paraules:

- Quin tipus d'atac poden fer al nostre servidor?

Ya que hasta el momento no hemos configurado nada relacionado con seguridad del servidor, podrían lanzar un envío de correos masivo para tumbar el servidor.

- Per què algú voldria atacar al servidor?

Para intentar robar la información de todos los correos de nuestra base de datos, para reventar nuestros servidores, etc.

- Com ho podríem solventar? 

Configurando algún sistema de seguridad en nuestro servidor.