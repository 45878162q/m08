![IIS-logo](images/iss-logo.png)

En esta práctica activarás el servicio IIS en un equipo con Windows 10 instalado, puede ser una máquina virtual o una máquia real, a tu elección.

[Aquí](https://dungeonofbits.com/activacion-de-internet-information-services-iis.html) tienes un tutorial para activar IIS en el equipo.

1.- Activa IIS en el equipo y documenta el proceso.

* Primero, necesitaremos acceder al panel de control de windows 10 y buscaremos "Programas y características".

* Una vez dentro del menú, haremos click en "Activar o desactivar las características de Windows. Dentro de la lista, buscaremos "Internet Information Services" y lo activaremos, daremos click en "Aceptar" y esperamos a que esté listo.

* Por último, cuando haya terminado el servicio estará listo para usarse.

2.- Crea un grupo de aplicaciones que se llame "APELLIDO_pool", donde APELLIDO es tu apellido.

![Alt](images/iis1.png)

3.- Cambia el nombre de la página por defecto que viene en IIS por "Smixers_APELLIDO", donde APELLIDO es tu apellido.

![Alt](images/iis2.png)

4.- Haz que la página "Smixers_APELLIDO" pertenezca al Application pool "APELLIDO_pool".

![Alt](images/iis3.png)

5.- ¿Si se colgase por algún error una página del "Application pool 1" se podría seguir accediendo a una página de otro Application pool?

* Si, ya que al estar en grupos distintos no afectaría que 1 de las 2 "Aplication pool" se colgasen.

6.- Limita el acceso al sitio "Smixers_APELLIDO" a 10 conexiones simultáneas y documenta el proceso.

* Para acceder a esta configuración haremos click derecho en el sitio de "Smixers_apellido", buscaremos "Administrar sitio web" y dentro de ahí "Límites".

* Una vez dentro del menú, activaremos la casilla "Límite de conexiones" y escribiremos el límite que queremos establecer, en nuestro caso 10.

![Alt](images/iis4.png)

7.- ¿El examen de directorios está habilitado por defecto en IIS?

* No, por defecto está deshabilitado, es necesario activarlo.

![Alt](images/iis5.png)

8.- ¿Qué indica el examen de directorios?

* Especifica la información que se muestra en el listado de directorios.

9.- ¿Cual es el directorio por defecto para el sitio de IIS?

El directorio por defecto es el que aparece debajo de la casilla "Ruta de acceso"

![Alt](images/iis6.png)

10.- Agrega un fichero html llamado "APELLIDO.html" al directorio por defecto de IIS, este documento contendrá tu CV (El que ya hiciste en M08) o uno nuevo en formato HTML. Si lo tienes en formato md puedes utilizar un conversor md a html online.

![Alt](images/iis7.png)

11.- Cambia las prioridades en "Documento predeterminado" del sitio para que cuando accedemos a "localhost" por el navegador muestre el curriculum del punto anterior en lugar de la página por defecto de IIS.

![Alt](images/iis8.png)

12.- Agrega un directorio virtual llamado Poblenou que se aloje en una carpeta llamada poblenou dentro del directorio por defecto de IIS.

![Alt](images/iis9.png)

13.- Crea una página html en la carpeta anterior que se llame index.html y contenga, al menos, tu apellido y la palabra Poblenou en ella.

![Alt](images/iis10.png)

14.- ¿Qué debes escribir en tu navegador para mostrar el fichero creado en el punto anterior?

* localhost/poblenou

15.- Crea una página de error 404 personalizada con texto e imágenes para tu sitio (puedes basarte en las de google u otro sitio web).

![Alt](images/iis12.png)

16.- Edita la configuración de tu sitio para que cuando se intente acceder a una página que no exista muestre la página que has creado en el punto anterior y documenta el proceso.

* Para establecer una página de error 404 personalizada, será necesario acceder  a la configuración del sitio web. Una vez hecho se escogerá el apartado "Páginas de errores"

* Una vez dentro, se hará click derecho en el error 404 y "modificar". Se escogerá la opción "Ejecutar una dirección URL en este sitio" y se escribirá la ruta donde se encuentre el mensaje de error personalizado.

![Alt](images/iis13.png)

* Por último, habrá que hacer click derecho en la parte superior del apartado "Página de errores" y "Modificar configuración de característica". Dentro, se seleccionará la opción "Página de errores personalizada" y se guardará la configuración. De esta forma ya se podrá utilizar la página de error personalizada

![Alt](images/iis14.png)

![Alt](images/iis11.png)

17.- Muestra el fichero log de tu sitio "Smixers_APELLIDO".

![Alt](images/iis15.png)
