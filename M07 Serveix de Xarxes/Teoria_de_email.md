# Teoria del correo electrónico

1.- Hem vist els protocols d’intercanvi de mails POP, IMAP i SMTP, però existeix
un altre protocol anomenat Exchange.

● Què és aquest protocol?

El protocolo exchange es una versión ampliada y mejorada del protocolo IMAP, esta nueva versión permite la sincronización de carpetas y la gestión del correo a través de varios dispositivos o equipos diferentes.

● Quina empresa el desenvolupa i utilitza?

Microsoft

● Quines diferències hi ha entre POP/IMAP i ExChange? Fes una taula amb
tres columnes per que es vegi més clar.

| | **POP3** | **IMAP** | **Exchange** |
| ------ | ------ | ------ | ------ |
| Los archivos se descargan en local por defecto | ✓ | X | X |
| Existe un límite de espacio por defecto | X | ✓ | ✓ |
| Sincronización de datos | X | ✓ | ✓ |
| Copias de seguridad en servidor | X | ✓ | ✓ |
| Accesible desde varios dispositivos | X | ✓ | ✓ |

2.- Busca dos programes de client de mail, un per a Windows i un per a Linux.

● Quins protocols utilitza cadascú.

Windows: Mailspring, este es un servicio de correo *freemium* el cuál se instala en el equipo. Utiliza los protocolos IMAP y POP.

Linux: Thunderbird es un servicio de correo libre que es necesario instalar en el equipo. Están disponibles los protocolos IMAP, POP y RSS

