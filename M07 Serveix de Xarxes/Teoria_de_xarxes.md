**1.** Què és una LAN i quines diferències hi ha amb una WAN? 2.- Busca quines són les capes del model OSI.


Una LAN (Local Area Network) és una xarxa local que acostuma a ser petita (Casa, Oficina, departament...). La seva diferència principal és que la WAN està formada per 2 o més LAN.

**Model OSI:**

7 - Aplicació

6 - Presentació

5 - Sessió

4 - Transport

3 - Xarxa

2 - Enllaç de dades

1 - Física

___

**3.** Quina és la unitat d'informació de les capes OSI: física, enllaç de dades, de xarxa i de transport.

La unitat d’informació es el BIT

___

**4.** Per a què serveix una adreça IP y una màscara de xarxa?

Una Adreça IP serveix per donar una identitat a un dispositiu connectat a una xarxa.

La màscara serveix per identificar dins de que xarxa està ubicat el dispositiu.

___

**5.** Explica les diferents classes d'adreçament IP: A, B, C. Indica per a cada classe el nombre de host que permet.

En una xarxa de classe A, s'assigna el primer octet per a identificar la xarxa, reservant els tres últims octets (24 bits) perquè siguin assignats als hosts, de manera que la quantitat màxima de hosts és 16 777 214 hosts.

En una xarxa de classe B, s'assignen els dos primers octets per a identificar la xarxa, reservant els dos octets finals (16 bits) perquè siguin assignats als hosts, de manera que la quantitat màxima de hosts és 5 534 hosts.

En una xarxa de classe C, s'assignen els tres primers octets per a identificar la xarxa, reservant l'octet final (8 bits) perquè sigui assignat als hosts, de manera que la quantitat màxima de hosts és 254 hosts.

___

**6.** Què són les classes D i E? Busca-les i explica-les.

La IP de classe D està reservat exclusivament per a multidifusió. Les adreces IP d’aquesta classe va de 224.0.0.0 fins a 239.255.255.255. La classe D no té màscara de subxarxa.

La IP de classe E està reservat per a finalitats experimentals només per a R&D o estudi. Les adreces IP d'aquesta classe va de 240.0.0.0 a 255.255.255.254. Com a Classe D, també aquesta classe no està equipada amb màscara de subxarxa.

___

**7.** Explica l’encapsulació dels paquets entre els diferents nivells. Per a què serveix?

Serveix per gestionar de forma més eficient les dades que s’envien.

___

**8.** Quina diferència hi ha entre les adreces públiques i privades. Quines són les adreces privades

L’adreça pública és l'identificador de la nostra xarxa des de l'exterior, és a dir, la del nostre encaminador de casa, que és el que és visible des de fora, mentre que la privada és la que identifica a cadascun dels dispositius connectats a la nostra xarxa. Les direccions privades són les que el router assigna al nostre ordinador, mòbil, tablet o qualsevol altre dispositiu que s'estigui connectat a ell.

___

**9.** Què són les adreces unicast, broadcast i multicast? Quines diferències hi ha entre elles?

Una adreça unicast està dirigida per un emisor fins un únic receptor.

Una adreça broadcast està dirigida per un emisor fins a tots els receptors possibles dins de la xarxa origen del host emissor.

Una adreça multicast està dirigida per un emisor fins a diversos receptors.

___

**10.** ¿Quines topologies de xarxa coneixes? 11.- Què és ARPANET?
Anell,
Arbre,
Bus,
Estrella i
Malla


ARPANET(Advanced Research Projects Agency Network) va ser una xarxa de computadores creada per encàrrec del Departament de Defensa dels Estats Units (DOD) per a utilitzar-la com a mitjà de comunicació entre les diferents institucions acadèmiques i estatals.