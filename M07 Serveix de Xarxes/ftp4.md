# Configurar un servei SFTP:

Una de les formes d'utilitzar FTP és mitjançant una connexió SSH, teniu un tutorial de com fer-ho [aquí](https://dungeonofbits.com/acceder-al-servidor-ftp-proftpd-como-sftp-por-ssh.html).

En aquesta pràctica instal·laràs un servei SSH al mateix equip que tens el servidor ProFTPD.

* Instal·la SSH al servidor.

![Alt](images/ftp29.png)

* Connecta't al servidor fent servir la comanda sftp.

![Alt](images/ftp30.png)

* Transfereix uns imatge del client al server.

![Alt](images/ftp31.png)

* Transfereix una altra imatge del server al client.

![Alt](images/ftp32.png)

* Mostra captures de pantalla de tots els punts anteriors.

