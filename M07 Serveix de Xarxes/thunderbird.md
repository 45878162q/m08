# Instalación de Thunderbird en Linux

Para instalar Thunderbird en el equipo será necesario acceder a un terminal y escribir el siguiente comando **"sudo apt install thunderbird"**.

![Alt](images/thunderbird1.png)

Es posible que después de introducir el comando aparezca el siguiente mensaje:

![Alt](images/thunderbird3.png)

Para solucionar lo anterior, será necesario escribir los 4 siguientes comando en la terminal:

· sudo fuser -vki  /var/lib/dpkg/lock

· sudo rm -f /var/lib/dpkg/lock

· sudo dpkg --configure -a

· sudo apt-get autoremove

Una vez hecho ya será posible instalar Thunderbird en el equipo.

___

# Configuración de Thunderbird

Con Thunderbird ya instalado, se abrirá un terminal y se ejecutará el comando **"thunderbird"**.

![Alt](images/thunderbird4.png)

Al hacerlo, aparecerá el siguiente menú:

![Alt](images/thunderbird5.png)

En el cuál se escribirán los datos del usuario que utilizará el correo.

![Alt](images/thunderbird6.png)

Es posible que aparezca la siguiente ventana con un error:

![Alt](images/thunderbird7.png)

Para solucionarlo, se tendrán que reescribir y reajustar los siguientes campos:

![Alt](images/thunderbird8.png)

Por último, al hacer click en "Volver a comprobar" y posteriormente a "Hecho", ya estará disponible el correo para poder utilizarlo.

![Alt](images/thunderbird9.png)