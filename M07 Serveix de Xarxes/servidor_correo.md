# 1.- Guia d'instal·lació del servidor Postfix+Dovecot+Squirrelmail.

### Postfix
Para comenzar a instalar postfix será necesario escribir en un terminal el comando "**sudo apt install postfix**".

![Alt](images/postfix1.png)

Después, aparecerá una pantalla indicando las configuraciones disponibles de postix, se seleccionará la opción "Sitio de internet".

![Alt](images/postfix2.png)

Por último, la instalación pedirá un nombre de dominio para el sitio, en este caso será candelario.com.

![Alt](images/postfix3.png)

## Dovecot

Para instalar Dovecot será necesario escribir en el terminal el comando "**sudo apt install dovecot-imapd dovecot-pop3d**".

![Alt](images/dovecot1.png)

Una vez hecho, se reiniciará el servicio con el comando "sudo service dovecot restart" y Dovecot habrá terminado de instalarse.

![Alt](images/dovecot2.png)

## Squirrelmail

Para descargar y preparar squirrelmail en el equipo, será necesario escribir en un terminal la siguiente línea "wget https://sourceforge.net/projects/squirrelmail/files/stable/1.4.22/squirrelmail-webmail-1.4.22.zip".

![Alt](images/squirrelmail1.png)

Una vez el paquete descargado, se ejecutará la siguiente sucesión de comando:

· **unzip squirrelmail-webmail-1.4.22.zip**

![Alt](images/squirrelmail2.png)

· **sudo mv squirrelmail-webmail-1.4.22 /var/www/html/**

![Alt](images/squirrelmail3.png)

· **sudo chown -R www-data:www-data /var/www/html/squirrelmail-webmail-1.4.22/**

![Alt](images/squirrelmail4.png)

· **sudo chmod 755 -R /var/www/html/squirrelmail-webmail-1.4.22/**

![Alt](images/squirrelmail5.png)

· **sudo mv /var/www/html/squirrelmail-webmail-1.4.22/ /var/www/html/squirrelmail**

![Alt](images/squirrelmail6.png)

Una vez hecho todos los pasos, squirrel estará listo para ser configurado.

# 1.1.- El domini serà X.com on X és el teu cognom.

![Alt](images/postfix3.png)

# 2.- Indicar quins protocols fa servir cada programa (Postfix, Dovecot i Squirrelmail).

· Postix utiliza el protocolo SMTP

· Dovecot utiliza los protocolos POP3 e IMAP

·Squirrelmail utiliza los protocolos IMAP, SMTP, TLS y SSL

# 3.- Crear dos usuaris de mail amb els noms dels vostres personatges de ficció preferits.

![Alt](images/servidorcorreo1.png)

![Alt](images/servidorcorreo2.png)

# 4.- Intercanviar un mail des de el primer personatge al segon i contestar al mail des del segon al primer i demostrar-ho visualment.

![Alt](images/servidorcorreo3.png)

![Alt](images/servidorcorreo4.png)

![Alt](images/servidorcorreo5.png)

![Alt](images/servidorcorreo6.png)
