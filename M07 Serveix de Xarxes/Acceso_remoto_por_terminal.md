## Teoria (5 punts): Explica amb les teves paraules.


- Quins ports utilitza Telnet per comunicar-se? I SSH?

Telnet utiliza el puerto 23 por defecto para establecer la conexión, mientras que SSH utiliza el 22. 

- Quines diferències hi ha entre Telnet i SSH?

La principal diferencia entre ellos es que, telnet no encripta las conexiones, por lo tanto cualquier tercero que esté conectado podrá ver todo el tráfico de telnet, mientras que ssh si que las encripta.

- Quina diferència hi he entre SSH1 i SSH2?

SSH2 es la versión mejorada de SSH1. Se encontraron vulnerabilidades en SSH1 y por lo tanto se creó esta nueva versión, la cuál tiene una algoritmo mejorado e incluye SFTP

- Amb quina instrucció copies un fitxer entre dos ordinadors utilitzant SSH?

La instrucción para copiar un archivo en ssh es **scp**, por ejemplo, para copiar un archivo desde tu ordenador al servidor se hará de la siguiente forma:

· scp archivo.txt dcandelario@dcandelario-VB:/home/dcandelario/

- Avantatges i inconvenients d'utilitzar un sistema d'accés remot de terminal o gràfic.

La ventaja de utilizar el terminal es que no será necesario descargar programas en el equipo, aunque al no tener entorno gráfico, aquellas personas que no estén relacionadas
con el terminal podrán tener problemas, por ello, el entorno gráfico será una mejor opción para ellas.

## Pràctica (5 punts): Demostra els següents passos amb captures on es vegi el teu nom.




- Instal·la openSSH a un equip amb Linux.

![Alt](images/ssh1.png)

- Crea un fitxer X.txt a l'equip amb Windows (X és el teu cognom).

![Alt](images/ssh2.png)

- Copia el fitxer que has creat des del teu equip fins a l'equip amb Linux mitjançant scp.

![Alt](images/ssh3.png)

- Obre una sessió SSH des d'un equip amb Windows sobre l'ordinador anterior.

![Alt](images/ssh4.png)

- A la sessió SSH ves a /home i fes una comanda ls.

![Alt](images/ssh5.png)

- Modifica el fitxer X.txt anomenant-lo XY.txt (on Y és el teu nom).

![Alt](images/ssh6.png)

![Alt](images/ssh7.png)

- Copia el fitxer modificat des de l'equip amb Linux fins a l'equip amb Windows utilitzant scp.

![Alt](images/ssh8.png)
