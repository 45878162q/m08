# Instalación de DNSMASQ:

## Pasos previos:

Para instalar el servicio DNS utilizando DNSMASQ en Ubuntu debemos deshabilitar systemd-resolved que es el sistema que lleva Ubuntu por defecto para resolver las peticiones a servidores DNS y caché:

*Paramos el servicio systemd-resolved:*

`sudo service systemd-resolved stop`

![Alt](images/systemstop.png)

*Deshabilitamos el servicio systemd-resolved:*

`sudo systemctl disable systemd-resolved`

![Alt](images/systemctl.png)

Por defecto en Linux el fichero **/etc/resolv.conf** está linkado a un fichero de configuración de systemd-resolved, debemos eliminar el enlace:

`sudo rm -v /etc/resolv.conf`

![Alt](images/nanorm.png)

Y ahora crearemos un nuevo fichero **/etc/resolv.conf** con la siguiente línea de texto *nameserver 8.8.8.8*

![Alt](images/nanoresolv.png)

![Alt](images/nameserver.png)

## Instalación de DNSMASQ:

Actualizaremos los paquetes de Ubuntu:

`sudo apt update`

![Alt](images/update.png)

Instalaremos DNSMASQ:

`sudo apt install dnsmasq`

![Alt](images/dnsmasq1.png)

![Alt](images/dnsmasq2.png)

Con esto ya queda instalado DNSMAQS.

## Configuración de DNSMASQ:

Para configurar nuestro dominio con DNSMASQ editaremos el fichero **/etc/dnsmasq.conf**, para ello guardaremos el fichero original.

`sudo cp /etc/dnsmasq.conf /etc/dnsmasc.conf.old`

![Alt](images/cp.png)

Después ya editamos el fichero en sí:

`sudo nano /etc/dnsmasq.conf`

![Alt](images/nano2.png)

Deberemos dejar el fichero de la siguiente forma:

`# DNS configuration`<br>
`port=53`<br>
` `<br>
`domain-needed`<br>
`bogus-priv`<br>
`strict-order`<br>
` `<br>
`expand-hosts`<br>
`domain=apellido.com`

![Alt](images/dnsconfig.png)

Donde el dominio *apellido.com* será vuestro propio apellido.com

Siempre que hagamos un cambio en el servidor reiniciaremos el mismo:

`sudo service dnsmasq restart`

![Alt](images/restart.png)

En el fichero **/etc/resolv.conf** tenemos las fuentes que consultará nuestro ordenador cuando vaya a resolver una dirección DNS, así que lo editaremos y pondremos la IP del PC con el servicio DNS al principio del fichero, justo antes de *nameserver 8.8.8.8*, para ello lo editamos con nano, por ejemplo y añadimos la siguiente línea:

*nameserver IP del servidor*

Por ejemplo si el servidor tiene la IP 10.0.2.15 pondremos:

*nameserver 10.0.2.15*

![Alt](images/10.0.2.15.png)

Y reiniciaremos el servicio.

## Añadiendo registros a DNSMASQ:

Para que nuestro servidor DNS reconozca los nombres de los equipos que queremos configurar necesitamos añadirlos de forma manual, para ello editamos **/etc/hosts**

`sudo nano /etc/hosts`

Y aquí añadimos los nombres de los equipos y su IP, por ejemplo podemos añadir la IP de nuestro servidor *10.0.2.15* junto a su nombre:

`# Registros DNS:`<br>
` `<br>
`10.0.2.15 servidor`<br>
`10.0.2.15 servidor.apellido.com`<br>

De esta manera podremos identificar eel nombre *servidor* y el nombre *servidor.apellido.com* con la misma IP 10.0.2.15

![Alt](images/hosts.png)

Podéis probar si funciona el servicio con el comando *dig* o *nslookup*

`dig servidor` <br>
`dig servidor.apellido.com`

![Alt](images/dig_nslookup.png)

## Personalizar el servidor DNS:

Crearemos una serie de registros DNS en nuestro servidor, para ello elegirás 10 dominios online y crearás un registro para cada uno de ellos, por ejemplo escogemos marca.es:

`193.110.128.199 marca`

![Alt](images/hosts2.png)

Comprueba que funcionan los registros con dig o nslookup a los registros creados.

Ahora, con mucho cuidado y **MUCHO MIMO** y sobre todo viendo que no tenemos un servicio *DHCP* activo, pondremos la màquina virtual en **Adaptador puente** para obtener una IP del aula.

Crearás un registro para cada compañero de clase con su IP y su nombre y comprobarás que puedes resolver el registro con dig o nslookup.

**IMPORTANTE:** Deberás comprobar que tienes la IP proporcionada por el servidor del Instituto para que estéis en la misma red todos los compañeros.

![Alt](images/ipscompa.png)

## Ampliación DNS:

Crea registros en el servidor DNS a los dominios:

* sega.com
* nintendo.es
* atari.com
* www.phenomena-experience.com
* sitgesfilmfestival.com

Para añadir los registros, será necesario abrir el archivo host, para ello, utilizaremos el comando **sudo gedit /etc/hosts** y agregar las siguientes lineas:

![Alt](images/ipsdhcp.png)

Y desde el *navegador web* de la propia máquina probarás si se pueden visitar dichos dominios.

**Documenta con imágenes y texto el proceso.**

¿Si queremos utilizar registros que no están en nuestro servidor DNS qué hemos de configurar en DNSMASQ?

Pon captura del fichero y lo que has añadido.

## Configuración de servicio DHCP en DNSMASQ:

Una vez que ya sabes configurar el servicio DNS de DNSMASQ configurarás el servicio DHCP también.

Para ello pasarás la MV servidor de DNSMASQ a "red NAT" y configurarás una IP estática para el servidor que será: 10.0.2.X (donde X es el último byte de la IP de tu máquina real).

Y crearás un pool de direcciones a proporcionar de la 10.0.2.200 a la 10.0.2.250.

**Documenta con imágenes y texto como lo has configurado.**

Primeramente, es necesario entrar en la configuración de red cableada. Después, accedemos a la configuración del cableado e introducimos los siguientes parámetros del apartado IPv4:

![Alt](images/cabconfig.png)

Para configurar el servicio dhcp es necesario crear el archivo **/etc/dnsmasq.d/dhcp.conf**, después, agregaremos las siguientes lineas en el:

![Alt](images/dhcp.conf.png)

Después utilizarás una MV cliente (también en "red NAT") para que use los servicios DHCP y DNS del servidor (Deberás usar su configuración de red).