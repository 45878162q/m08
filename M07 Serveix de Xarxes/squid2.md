# Instal·lar sarg per a Squid:

En aquesta pràctica has d'instal·lar sarg per a Squid. Tens un [tutorial aquí](https://dungeonofbits.com/instalacion-y-configuracion-de-sarg-para-squid.html).

1. Explica com has instal·lat sarg.

Para instalar sarg en el equipo, solo es necesario escribir en el terminal de comandos **"sudo apt install sarg"**, una vez hecho comenzará el proceso de instalación. No será necesario realizar ningún otro paso

![Alt](images2/sarg1.png)

Como paso extra, será necesario realizar unos cambios en las configuración del archivo .conf. Para ello, se ejecutará la instrucción sudo nano **"/etc/sarg/sarg.conf"** y se cambirán las líneas 120 y 121, de igual forma que se muestra en la captura inferior.

![Alt](images2/sarg2.png)

Por último se tendrán que realizar otros pequeños cambios en el archivo de configuración, en las líneas 132, 231, 377. El resultado será el mismo que los de las imágenes inferiores.

![Alt](images2/sarg4.png)

![Alt](images2/sarg3.png)

![Alt](images2/sarg5.png)

Demana a algun company (o fes servir diferents equips) que es connecti a Internet mitjançant el teu proxy Squid.

2. Mostra una captura de access.log amb les connexions.

![Alt](images2/sarg6.png)

3. Mostra una captura de sarg amb les mateixes connexions. 
 
![Alt](images2/sarg7.png)