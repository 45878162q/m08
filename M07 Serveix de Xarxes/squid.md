# Pràctica Proxy Squid 1:

Ara que ja saps instal·lar i configurar el proxy Squid [recorda el tutorial que tens aquí](https://dungeonofbits.com/instalacion-de-squid-en-linux.html) podem començar a treballar en el nostre propi proxy.

Per orientar-te tens la documentació de Squid [aquí](https://wiki.squid-cache.org/SquidFaq/SquidAcl#Access_Controls_in_Squid)

Potser hauràs de canviar la configuració de squid.conf per a cada punt, així que serà millor que guardis una copia de seguretat cada vegada amb la configuració funcionant i facis les captures abans de canviar-la.

## 1.- Instal·lació:

	Instal·la Squid (mostra una captura del proxy funcionant i que al terminal es vegi el teu usuari):

![Alt](images2/squid1.png)

## 2.- Configurar accés per IP:

	Mostra captures que demostrin els següents punts:

	*Crea un fitxer anomenat permesos.txt amb les IPs dels equips de dos companys de classe.
	
![Alt](images2/squid2.png)
	
	*Crea un fitxer anomenat prohibits.txt amb les IPs dels equips de dos companys de classe. 
	
![Alt](images2/squid3.png)
	
	*Crea una ACL anomenada X_seal_of_aprovement (on X és el teu cognom) amb les IPs del fitxer permesos.txt
	
![Alt](images2/squid4.png)

	*Crea una ACL anomenada X_death_note (on X és el teu cognom) amb les IPs del fitxer prohibits.txt
	
![Alt](images2/squid5.png)

	*Permet les connexions dels equips de la llista X_seal_of_aprovement.

![Alt](images2/squid6.png)

	*Prohibeix les connexions dels equips de la llista X_death_note.

![Alt](images2/squid7.png)

	*Reinicia el proxy i comprova amb els companys que els equips de la llista permesos tenen accés a Internet mitjançant el teu proxy i la resta d'equips no poden accedir.

![Alt](images2/squid8.png)

![Alt](images2/squid9.png)

![Alt](images2/squid10.png)

	*Comprova amb el mestre si ell pot accedir des de el seu ordinador a Internet mitjançant el teu proxy. 
	
![Alt](images2/dani1.png)
	
	*Explica perquè té accés o no amb les teves paraules i mostrant una captura de squid.conf on es demostri el que dius.

![Alt](images2/squid14.png)

Tiene acceso porque la configuración del proxy permite el acceso a cualquier usuario que no se encuentre denegado, mediante el parámetro **"http_access allow all"**

## 3.- Configurar dominis permesos i prohibits:

	Mostra captures que demostrin els següents punts:

	*Crea un fitxer anomenat ban.txt amb 10 dominis que no es podran accedir mitjançant el teu proxy.

![Alt](images2/squid11.png)
	
	*Crea una ACL que es digui X_banned_domains (on X és el teu cognom), amb els dominis del fitxer ban.txt.
	
![Alt](images2/squid12.png)
	
	*Crea una norma que prohibeixi l'accés als dominis de la ACL X_banned_domains als usuaris del proxy.
	
![Alt](images2/squid13.png)
	
	*Comprova amb dos companys que tinguin accés a connexions al teu proxy que poden accedir a tots els dominis menys els de la llista X_banned_domains.



	*Fes la prova amb el mestre.



## 4.- Configurar patrons de url prohibits:

	Mostra captures que demostrin els següents punts:
	
	*Crea una ACL anomenada X_banned_words que contingui una llista de paraules (mínim 10).

![Alt](images2/squid15.png)

	*Crea una norma que faci que qualsevol url que contingui una paraula de la llista X_banned_words sigui bloquejada pel proxy.
	
![Alt](images2/squid16.png)
	
	*Demostra-ho amb un company.
	
![Alt](images2/DanielProxeneta.png)
	
	*Fes la prova amb el mestre.
	


## 5.- Accés exclusiu:

	Mostra captures que demostrin els següents punts:
	
	*Crea una ACL amb la IP del mestre.
	
![Alt](images2/squid18.png)

	*Crea una ACL amb la URL /var/www/html/mestre/index.html on aquest index.html serà el teu CV.

![Alt](images2/squid19.png)

	*Fes que només la IP del mestre pugui accedir a la url.
	
![Alt](images2/squid20.png)

	
	*Demostra-ho amb un company.
	
![Alt](images2/Dani.png)
	
	*Demostra-ho amb l'equip local del proxy.
	
![Alt](images2/squid21.png)	
	
	*Demostra-ho amb el mestre.
	

## 6.- Log de Squid:
 
El log del servei Squid es trova al directori: /var/log/squid/. 

* Mostra captures que demostrin els següents punts:
	
* Mostra captura del log indicant clarament els intents de connexió del punt 5 (Connexió rebutjada al equip del company, rebutjada al equip local, aprovada per a l'equip del mestre).


## 7.- Horari de connexió:

Es poden crear horaris dins la configuració de Squid, la forma de  fer-ho és dient-li a Squid els dies de la setmana i l'interval d'hores, per exemple:

```
acl horari_smx_tarda MTWHF 15:00-21:30
```

Aquesta acl és un interval de hores de les 3 de la tarda fins les 9 i mitja del vespre de dilluns a divendres.

Els dies de la setmana a Squid són, de dilluns a diumenge: MTWHFAS.

Les hores van en format de 24 hores: 00:00 a 23:59

* Crea una restricció amb l'horari de les classes de SMX 2T de M07, fent que a aquelles hores només es pugui accedir a gitlab.com, dungeonofbits.com i iespoblenou.org
* Copia aqui les acl i les restriccions.

acl horario_m07_lunes time MTWHF 16:00-18:00

acl m07 dstdomain "/etc/squid/m07.txt"

http_access allow m07 horario_m07_lunes

http_access deny all

* Comprova si pots accedir a google.com (NO) i a gitlab.com (SI) i mostra la captura de /var/log/acces.log que ho demostra.

![Alt](images2/squid22.png)	
    
## 8.- Resum de configuracions de Squid:

En aquest apartat només has de copiar el contingut de squid.conf per als següents casos:

1. A la teva empresa els usuaris de la xarxa 192.168.1.0/24 només poden accedir a youtube.com de 13:00 a 15:00 de dilluns a dijous (que és l'horari de dinar, perquè els divendres a la tarda no es treballa). Durant la resta de l'horari no es pot accedir a youtube.com però sí a la resta de webs.

acl comida time MTWH 13:00-15:00

acl youtube dstdomain "/etc/squid/youtube.txt"

http_access allow youtube comida

http_access deny all

2. Has detectat que els teus usuaris accedeixen a pàgines d'apostes i el teu cap no vol que ho puguin fer, l'únic problema és que el teu cap SÍ QUE VOL ACCEDIR a aquestes pàgines. Busca 5 pàgines d'empreses de juguesques online i crea una acl amb elles, deixa que el teu cap a la IP 172.31.84.197 pugui accedir a la llista però que la resta d'usuaris no.  Tota la resta de pàgines web són accesibles.


acl apuesta dstdomain "/etc/squid/apuesta.txt"

acl permitir src 172.31.84.197

http_access deny apuesta

http_access allow permitir

http_access allow all

3. Tens una empresa de productes vegans i vols evitar que els usuaris de la xarxa accedeixin a cap pàgina que parli de productes càrnics. Fes una llista de 5 paraules de productes no aptes per a vegans i prohibeix la búsqueda d'aquestes paraules com a part de dominis als usuaris. Tota la resta de pàgines web són accesibles.

acl veganosalpoder url_regex "/etc/squid/veganos.txt"

http_access deny veganosalpoder

http_access allow all

4. La empresa TRINIDAD té dos CEOs, en Bud que es fan del Barça i el Terence, que és fan de l'Espanyol, per aquest motiu a la seva empresa només es pot accedir a la web preferida per Bud, www.fcbarcelona.es, els dilluns, dimecres i divendres de 08:00 a 20:00. Els dimarts i dijous només es pot accedir a www.rcdespanyol.com, l'equip favorit del Terence al mateix marc horari. Els caps de setmana només es pot accedir a la web www.realzaragoza.com durant tot el dia perquè és l'equip preferit del vigilant de seguretat.


acl barça time MWF 08:00-20:00

acl español time TH 08:00-20:00

acl zaragoza time AS 00:00-23:59

acl barcelona dstdomain "/etc/squid/barcelona.txt"

acl rcespañol dstdomain "/etc/squid/rcespañol.txt"

acl realzaragoza dstdomain "/etc/squid/realzaragoza.txt"

http_access allow barcelona barça

http_access allow rcespañol español

http_access allow realzaragoza zaragoza

http_access deny all




	
