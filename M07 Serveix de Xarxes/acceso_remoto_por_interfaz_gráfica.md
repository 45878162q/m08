En aquesta pràctica utilitzarem un software anomenat Teamviewer per a controlar de forma remota un altre equip.

Teamviewer és gratuït per a ús no comercial, el podeu descarregar des de la seva web teamviewer.com.

Teniu un tutorial de com instal·lar-lo a Linux aquí: https://dungeonofbits.com/instalar-teamviewer-en-linux.html Instal·lar-lo a Windows és trivial.



## Enunciat:

Documenta amb text i/o captures.

-  Instal·la Teamviewer a un equip amb Windows.

![Alt](images/teamviewer1.png)

 - Instal·la Teamviewer a un equip amb Linux.

![Alt](images/teamviewer2.png)

Els dos equips han de tenir connectivitat per xarxa.

-  Obre Teamviewer als dos equips i fes que l'usuari de l'equip amb Windows pugui controlar el equip amb Linux.

![Alt](images/teamviewer3.png)

-  Crea de forma remota utilitzant Teamviewer un fitxer a /home/usuari un fitxer amb la data i hora actuals anoment X.txt (on X és el teu cognom).

![Alt](images/teamviewer4.png)

-  Utilitza la transferència de fitxers de Teamviewer per transferir el fitxer de l'equip amb Linux a l'equip amb Windows.

![Alt](images/teamviewer5.png)

-  Modifica el fitxer  X.txt afegint una altra línia amb la data i hora actuals.

![Alt](images/teamviewer6.png)

-  Utilitza la transferència de fitxers de Teamviewer per transferir el fitxer de l'equip amb Windows a l'equip amb Linux.

![Alt](images/teamviewer7.png)

-  Podem utilitzar Teamviewer de forma gratuïta per la nostra empresa de manteniment d'equips informàtics?

No, Teamviewer solo se puede utilizar gratuitamente de forma personal, para utilizarlo de forma empresarial es necesario pagar licencias.

-  Investiga i busca per Internet un altre software d'accés remot que sigui multiplataforma i digues el nom, la web i la url de descàrrega.

Real VNC es otro programa con el objetivo de conectarse remotamente a otro equipo y se encuentra disponible para Windows, Linux y Mac.

[Página Web](https://www.islonline.com/es/es/) / [Página de descargas](https://www.islonline.com/es/es/downloads/)

Investiga be perquè la següent pràctica hauràs d'utilitzar aquest software.
